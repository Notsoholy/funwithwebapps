﻿using Miac.Persons.Abstraction;
using Miac.Persons.Domain;
using Miac.Persons.Infrastructure.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miac.Persons.Infrastructure
{
    public class EFPersonsRepository : IPersonsRepository
    {
        private readonly PersonsContext _context;

        public EFPersonsRepository(PersonsContext context)
        {
            _context = context ?? throw new ArgumentNullException(nameof(context));
        }

        public Person Add(Person person)
        {
            _context.Persons.Add(person);
            _context.SaveChanges();
            return person;
        }

        public Person Get(int id)
        {
            return _context.Persons.Find(id);
        }

        public IReadOnlyCollection<Person> Get()
        {
            return _context.Persons.ToList();
        }
    }
}
