﻿using Miac.Persons.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Miac.Persons.Infrastructure.EntityFramework
{
    public class PersonsContext : DbContext
    {
        public DbSet<Person> Persons { get; set; }

        public PersonsContext(DbContextOptions<PersonsContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
