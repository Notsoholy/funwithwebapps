﻿using Miac.Persons.Abstraction;
using Miac.Persons.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Miac.Persons.Infrastructure
{
    public class StaticPersonsRepository : IPersonsRepository
    {
        private static readonly List<Person> Persons = new List<Person>();

        public Person Add(Person person)
        {
            person.Id = Persons.Count + 1;
            Persons.Add(person);
            return person;
        }

        public Person Get(int id)
        {
            return Persons.FirstOrDefault(person => person.Id == id);
        }

        public IReadOnlyCollection<Person> Get()
        {
            return Persons;
        }
    }
}
