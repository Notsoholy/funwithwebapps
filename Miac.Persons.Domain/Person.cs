﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Miac.Persons.Domain
{
    public class Person
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
