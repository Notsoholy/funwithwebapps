﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Miac.Times.Services
{
    public class DateTimeFormatOptions
    {
        public List<string> AllowedFormats { get; set; }
    }
}
