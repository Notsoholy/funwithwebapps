﻿using Miac.Times.Abstractions.Services;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace Miac.Times.Services
{
    public class DateTimeFormatProvider : IDateTimeFormatProvider
    {
        private readonly ILogger<DateTimeFormatProvider> _logger;
        private readonly DateTimeFormatOptions _options;

        public DateTimeFormatProvider(
            ILogger<DateTimeFormatProvider> logger,
            IOptions<DateTimeFormatOptions> options)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _options = options.Value ?? throw new ArgumentNullException(nameof(options));
        }

        public IReadOnlyCollection<string> GetAllowedFormats()
        {
            _logger.LogInformation("Allowed formats requested. Allowed formats are: [{0}]", _options.AllowedFormats);
            return _options.AllowedFormats;
        }

        public bool IsFormatAllowed(string format)
        {
            var isAllowed = _options.AllowedFormats.Contains(format);
            _logger.LogInformation("Checking if format allowed. Format allowed: {0}", isAllowed);
            return isAllowed;
        }
    }
}
