﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Miac.Times.Services
{
    public class DateTimeOffsetOptions
    {
        public int MaxOffsetMinutes { get; set; } = 840;

        public int MinOffsetMinutes { get; set; } = -720;

        public int OffsetMultiplier { get; set; } = 30;
    }
}
