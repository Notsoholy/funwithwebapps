﻿using Miac.Times.Abstractions.Services;
using Miac.Times.Domain.Exceptions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace Miac.Times.Services
{
    public class DateTimeWithOffsetProvider : IDateTimeWithOffsetProvider
    {
        private readonly ILogger<DateTimeWithOffsetProvider> _logger;

        private readonly DateTimeOffsetOptions _offsetOptions;

        private readonly DateTimeFormatOptions _formatOptions;

        public DateTimeWithOffsetProvider(
            ILogger<DateTimeWithOffsetProvider> logger,
            IOptions<DateTimeOffsetOptions> offsetOptions,
            IOptions<DateTimeFormatOptions> formatOptions)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _offsetOptions = offsetOptions?.Value ?? throw new ArgumentNullException(nameof(offsetOptions));
            _formatOptions = formatOptions?.Value ?? throw new ArgumentNullException(nameof(formatOptions));
        }

        private void ValidateFormat(string format)
        {
            if (!_formatOptions.AllowedFormats.Contains(format))
            {
                _logger.LogInformation("Passed format [{0}] is not allowed by options.", format);
                throw new FormatNotAllowedException($"Format [{format}] is not allowed.");
            }
        }

        public string OffsetDateTimeWithFormat(int offset = 0, string format = "")
        {
            ValidateFormat(format);

            ValidateOffset(offset);

            DateTime utc = DateTime.UtcNow;
            string currentTime = utc.AddMinutes(offset).ToString(format, CultureInfo.InvariantCulture);

            _logger.LogInformation("UTC: {0} | Offset: {1} | DateTime: {2}", utc, offset, currentTime);
            return currentTime;
        }

        private void ValidateOffset(int offset)
        {
            if (offset > _offsetOptions.MaxOffsetMinutes)
            {
                _logger.LogInformation("Offset too large: {0}. Max offset: {1}", offset, _offsetOptions.MaxOffsetMinutes);
                throw new TooLargeOffsetException("Offset is too large.");
            }

            if (offset < _offsetOptions.MinOffsetMinutes)
            {
                _logger.LogInformation("Offset too small: {0}. Min offset: {1}", offset, _offsetOptions.MinOffsetMinutes);
                throw new TooSmallOffsetException("Offset is too small.");
            }

            if (offset % _offsetOptions.OffsetMultiplier != 0)
            {
                _logger.LogInformation("Offset is invalid: {0}", offset);
                throw new OffsetInvalidException($"Offset must be divisible by {_offsetOptions.OffsetMultiplier}.");
            }
        }
    }
}
