﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiacWebApp.Requests
{
    public class CreatePersonRequest
    {
        public string Name { get; set; }
    }
}
