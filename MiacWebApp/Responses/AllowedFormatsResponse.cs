﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiacWebApp.Responses
{
    public class AllowedFormatsResponse
    {
        public IEnumerable<string> Allowed { get; set; }
    }
}
