using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Miac.Persons.Abstraction;
using Miac.Persons.Infrastructure;
using Miac.Persons.Infrastructure.EntityFramework;
using Miac.Times.Abstractions.Services;
using Miac.Times.Services;
using MiacWebApp.Controllers;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace MiacWebApp
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration ?? throw new ArgumentNullException(nameof(configuration));
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddScoped<IPersonsRepository, EFPersonsRepository>()
                .AddScoped<IDateTimeWithOffsetProvider, DateTimeWithOffsetProvider>()
                .AddScoped<IDateTimeFormatProvider, DateTimeFormatProvider>()
                .Configure<DateTimeOffsetOptions>(Configuration.GetSection(nameof(DateTimeOffsetOptions)))
                .Configure<DateTimeFormatOptions>(Configuration.GetSection(nameof(DateTimeFormatOptions)))
                .AddControllers();
            ConfigureDatabaseServices(services);
        }

        private void ConfigureDatabaseServices(IServiceCollection services)
        {
            services.AddDbContext<PersonsContext>(builder =>
            {
                builder.UseSqlServer(Configuration.GetConnectionString("PersonsContext"));
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapGet("/date", async context =>
                {
                    await context.Response.WriteAsync(DateTime.Now.ToString());
                });
            });

        }
    }
}
