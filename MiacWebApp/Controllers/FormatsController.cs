﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Miac.Times.Abstractions.Services;
using MiacWebApp.Responses;
using Microsoft.AspNetCore.Mvc;

namespace MiacWebApp.Controllers
{
    [Route("datetime/[controller]")]
    [ApiController]
    public class FormatsController : Controller
    {
        private readonly IDateTimeFormatProvider _provider;

        public FormatsController(IDateTimeFormatProvider provider)
        {
            _provider = provider ?? throw new ArgumentNullException(nameof(provider));
        }

        [HttpGet]
        public IActionResult GetAllowedFormats()
        {
            var response = new AllowedFormatsResponse
            {
                Allowed = _provider.GetAllowedFormats()
            };
            return Ok(response);
        }

        [HttpGet("isAllowed")]
        public IActionResult IsFormatAllowed([FromQuery] string format)
        {
            var response = _provider.IsFormatAllowed(format);
            return Ok(response);
        }
    }
}