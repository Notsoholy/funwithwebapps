﻿using Miac.Persons.Abstraction;
using Miac.Persons.Domain;
using MiacWebApp.Requests;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiacWebApp.Controllers
{
    [Route("[controller]")]
    [ApiController]

    public class PersonsController : Controller
    {
        private readonly IPersonsRepository _repository;

        public PersonsController(IPersonsRepository repository)
        {
            _repository = repository ?? throw new ArgumentNullException(nameof(repository));
        }

        [HttpPost]
        public IActionResult Create([FromBody] CreatePersonRequest request)
        {
            var person = new Person
            {
                Name = request.Name
            };
            person = _repository.Add(person);
            return Created("", person);
        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var person = _repository.Get(id);
            if (person == null)
            {
                return NoContent();
            }
            return Ok(person);
        }

        [HttpGet]
        public IActionResult Get()
        {
            var persons = _repository.Get();

            return Ok(persons);
        }
    }
}
