﻿using System;
using Miac.Times.Abstractions.Services;
using Miac.Times.Domain.Exceptions;
using Microsoft.AspNetCore.Mvc;

namespace MiacWebApp.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class DateTimeController : Controller
    {
        public string Index()
        {
            return DateTime.UtcNow.ToString();
        }

        [HttpGet("current")]
        public IActionResult Current(
            [FromServices] IDateTimeWithOffsetProvider provider,
            [FromQuery] int offset = 0,
            [FromQuery] string format = "")
        {
            try
            {
                return Ok(provider.OffsetDateTimeWithFormat(offset, format));
            }   
            catch(OffsetInvalidException e)
            {
                return BadRequest(e.Message);
            }
            catch(FormatNotAllowedException e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}