﻿using Miac.Persons.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace Miac.Persons.Abstraction
{
    public interface IPersonsRepository
    {
        Person Add(Person person);
        Person Get(int id);
        IReadOnlyCollection<Person> Get();
    }
}
