﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Miac.Times.Domain.Exceptions
{
    public class TooLargeOffsetException : OffsetInvalidException
    {
        public TooLargeOffsetException()
        {
        }

        public TooLargeOffsetException(string message) : base(message)
        {
        }
    }
}
