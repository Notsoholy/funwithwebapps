﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Miac.Times.Domain.Exceptions
{
    public class TooSmallOffsetException : OffsetInvalidException
    {
        public TooSmallOffsetException()
        {
        }

        public TooSmallOffsetException(string message) : base(message)
        {
        }
    }
}
