﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Miac.Times.Domain.Exceptions
{
    public class FormatNotAllowedException : Exception
    {
        public FormatNotAllowedException()
        {
        }

        public FormatNotAllowedException(string message) : base(message)
        {
        }
    }
}
