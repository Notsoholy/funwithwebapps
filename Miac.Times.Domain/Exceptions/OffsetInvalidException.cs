﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Miac.Times.Domain.Exceptions
{
    public class OffsetInvalidException : Exception
    {
        public OffsetInvalidException()
        {
        }

        public OffsetInvalidException(string message) : base(message)
        {
        }
    }
}
