﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Miac.Times.Abstractions.Services
{
    public interface IDateTimeFormatProvider
    {
        IReadOnlyCollection<string> GetAllowedFormats();

        bool IsFormatAllowed(string format);
    }
}
