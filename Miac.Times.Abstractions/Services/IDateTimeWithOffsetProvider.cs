﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Miac.Times.Abstractions.Services
{
    public interface IDateTimeWithOffsetProvider
    {
        string OffsetDateTimeWithFormat(int offset = 0, string format = "");
    }
}
