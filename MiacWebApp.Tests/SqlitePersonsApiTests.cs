﻿using Xunit;

namespace MiacWebApp.Tests
{
    public class SqlitePersonsApiTests : PersonsApiTests
    {
        public SqlitePersonsApiTests() : base(new SqliteWebApplicationFactory())
        {
        }
    }
}
