﻿using MiacWebApp.Requests;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MiacWebApp.Tests
{
    public class PersonsApiClient
    {
        private readonly HttpClient _client;

        public PersonsApiClient(HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<HttpResponseMessage> GetAsync(int id)
        {
            return await _client.GetAsync($"/persons/{id}");
        }

        public async Task<HttpResponseMessage> GetAsync()
        {
            return await _client.GetAsync("/persons");
        }

        public async Task<HttpResponseMessage> PostAsync(CreatePersonRequest request)
        {
            var content = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
            return await _client.PostAsync("/persons", content);
        }
    }
}
