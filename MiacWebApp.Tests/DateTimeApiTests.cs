﻿using FluentAssertions;
using FluentAssertions.Extensions;
using Miac.Times.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xunit;

namespace MiacWebApp.Tests
{
    public abstract class DateTimeApiTests : IDisposable
    {
        protected WebApplicationFactory<Startup> _factory;

        protected DateTimeApiClient _client;

        public DateTimeApiTests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = new DateTimeApiClient(_factory.CreateClient());
        }

        [Fact]
        public async Task Current_ByDefault_ReturnsUtc()
        {
            var expectedUtc = DateTime.UtcNow;

            var response = await _client.GetAsync();

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var body = await response.Content.ReadAsStringAsync();
            var actualDateTime = DateTime.Parse(body, CultureInfo.InvariantCulture);
            actualDateTime.Should().BeCloseTo(expectedUtc, 1000);
        }

        [Theory]
        [InlineData(0, HttpStatusCode.OK)]
        [InlineData(30, HttpStatusCode.OK)]
        [InlineData(60, HttpStatusCode.OK)]
        [InlineData(61, HttpStatusCode.BadRequest)]
        public async Task Current_WithOffset_ValidateOffset(int offset, HttpStatusCode expectedStatusCode)
        {
            var response = await _client.GetAsync(offset);

            response.StatusCode.Should().Be(expectedStatusCode);
        }

        [Fact]
        public async Task Current_TooLargeOffset_ReturnsBadRequest()
        {
            var options = _factory.Services.GetRequiredService<IOptions<DateTimeOffsetOptions>>().Value;
            var tooLargeOffset = options.MaxOffsetMinutes + options.OffsetMultiplier;
            var expectedStatusCode = HttpStatusCode.BadRequest;

            var response = await _client.GetAsync(tooLargeOffset);
            
            response.StatusCode.Should().Be(expectedStatusCode);
        }
        
        [Fact]
        public async Task Current_TooSmallOffset_ReturnsBadRequest()
        {
            var options = _factory.Services.GetRequiredService<IOptions<DateTimeOffsetOptions>>().Value;
            var tooSmallOffset = options.MinOffsetMinutes - options.OffsetMultiplier;
            var expectedStatusCode = HttpStatusCode.BadRequest;

            var response = await _client.GetAsync(tooSmallOffset);
            
            response.StatusCode.Should().Be(expectedStatusCode);
        }

        [Fact]
        public async Task Current_Format_ReturnsddMMyyyy()
        {
            var expectedFormat = new Regex("\\d\\d\\.\\d\\d\\.\\d\\d\\d\\d");
            var inputFormat = "dd.MM.yyyy";

            var response = await _client.GetAsync(inputFormat);

            var body = await response.Content.ReadAsStringAsync();

            body.Length.Should().Be(expectedFormat.Match(body).Length);
        }

        [Fact]
        public async Task Current_Format_ReturnsDateWithSlashSeparator()
        {
            var expectedFormat = new Regex("\\d\\d\\/\\d\\d\\/\\d\\d\\d\\d");
            var inputFormat = "dd/MM/yyyy";

            var response = await _client.GetAsync(inputFormat);

            var body = await response.Content.ReadAsStringAsync();

            body.Length.Should().Be(expectedFormat.Match(body).Length);
        }

        [Fact]
        public async Task Current_InvalidFormat_ReturnsBadRequest()
        {
            var invalidFormat = "MM/dd/yyyy";

            var response = await _client.GetAsync(invalidFormat);

            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        public void Dispose()
        {
            _factory.Dispose();
        }
    }
}
