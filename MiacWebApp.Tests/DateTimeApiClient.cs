﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MiacWebApp.Tests
{
    public class DateTimeApiClient
    {
        private readonly HttpClient _client;

        public DateTimeApiClient(HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<HttpResponseMessage> GetAsync()
        {
            return await _client.GetAsync("datetime/current");
        }

        public async Task<HttpResponseMessage> GetAsync(int offset)
        {
            return await _client.GetAsync($"datetime/current?offset={offset}");
        }

        public async Task<HttpResponseMessage> GetAsync(string format)
        {
            return await _client.GetAsync($"datetime/current?format={format}");
        }

        public async Task<HttpResponseMessage> GetAsync(int offset, string format)
        {
            return await _client.GetAsync($"datetime/current?offset={offset}&format={format}");
        }
    }
}
