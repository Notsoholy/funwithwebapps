﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using FluentAssertions;
using FluentAssertions.Extensions;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net;
using Miac.Persons.Domain;
using MiacWebApp.Requests;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Testing;

namespace MiacWebApp.Tests
{
    public abstract class PersonsApiTests : IDisposable
    {
        protected WebApplicationFactory<Startup> _factory;

        protected PersonsApiClient _client;

        public PersonsApiTests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = new PersonsApiClient(_factory.CreateClient());
        }

        [Theory]
        [InlineData("WOWA")]
        [InlineData("Vasily Antonovich")]
        [InlineData("Conrad Kerz")]
        public async Task Create_ByDefault_ReturnsCreatedPerson(string name)
        {
            var request = new CreatePersonRequest
            {
                Name = name
            };
            var response = await _client.PostAsync(request);

            response.StatusCode.Should().Be(HttpStatusCode.Created);

            var body = await response.Content.ReadAsStringAsync();
            var createdPerson = JsonConvert.DeserializeObject<Person>(body);

            createdPerson.Should().NotBeNull();
            createdPerson.Id.Should().NotBe(default);
            createdPerson.Name.Should().Be(name);
        }

        [Theory]
        [InlineData("WOWA")]
        [InlineData("Vasily Antonovich")]
        [InlineData("Conrad Kerz")]
        public async Task Get_WhenPersonExists_ReturnsPersonById(string name)
        {
            var request = new CreatePersonRequest
            {
                Name = name
            };
            var createResponse = await _client.PostAsync(request);

            createResponse.StatusCode.Should().Be(HttpStatusCode.Created);

            var body = await createResponse.Content.ReadAsStringAsync();
            var createdPerson = JsonConvert.DeserializeObject<Person>(body);

            createdPerson.Should().NotBeNull();
            createdPerson.Id.Should().NotBe(default);
            createdPerson.Name.Should().Be(name);

            var getResponse = await _client.GetAsync(createdPerson.Id);

            getResponse.StatusCode.Should().Be(HttpStatusCode.OK);

            body = await getResponse.Content.ReadAsStringAsync();
           
            var receivedPerson = JsonConvert.DeserializeObject<Person>(body);

            receivedPerson.Should().NotBeNull();
            receivedPerson.Id.Should().Be(createdPerson.Id);
            receivedPerson.Name.Should().Be(createdPerson.Name);

        }

        [Fact]
        public async Task Get_NotExistingId_ReturnsNoContent()
        {
            var response = await _client.GetAsync(0);
            response.StatusCode.Should().Be(HttpStatusCode.NoContent);
        }

        [Fact]
        public async Task Get_ByDefault_ReturnsEmptyList()
        {
            var response = await _client.GetAsync();

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var body = await response.Content.ReadAsStringAsync();
            var persons = JsonConvert.DeserializeObject<List<Person>>(body);

            persons.Should().BeEmpty();
        }

        [Fact]
        public async Task Get_AfterAddingPersons_ReturnsCorrectList()
        {
            var names = new string[] { "Poopa", "Loopa" };
            var requests = names.Select(name => new CreatePersonRequest { Name = name });
            foreach(var request in requests)
            {
                await _client.PostAsync(request);
            }
            var response = await _client.GetAsync();

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var body = await response.Content.ReadAsStringAsync();
            var persons = JsonConvert.DeserializeObject<List<Person>>(body);
            persons.Should().HaveSameCount(requests);
        }

        public void Dispose()
        {
            _factory.Dispose();
        }
    }
}
