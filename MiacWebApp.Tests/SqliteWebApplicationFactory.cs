﻿using Miac.Persons.Infrastructure.EntityFramework;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace MiacWebApp.Tests
{
    public class SqliteWebApplicationFactory : BaseWebApplicationFactory<Startup>
    {
        private const string InMemorySqliteConnectionString = "DataSource=:memory:";
        
        private readonly SqliteConnection _connection;

        public SqliteWebApplicationFactory()
        {
            _connection = new SqliteConnection(InMemorySqliteConnectionString);
            _connection.Open();
        }

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            base.ConfigureWebHost(builder);
            builder.ConfigureServices(services =>
            {
                services
                    .AddEntityFrameworkSqlite()
                    .AddDbContext<PersonsContext>(options =>
                    {
                        options
                            .UseSqlite(_connection)
                            .UseInternalServiceProvider(services.BuildServiceProvider());
                    });
            });
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            _connection.Close();
        }
    }
}
