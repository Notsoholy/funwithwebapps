﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MiacWebApp.Tests
{
    public class FormatsApiClient
    {
        private readonly HttpClient _client;

        public FormatsApiClient(HttpClient client)
        {
            _client = client ?? throw new ArgumentNullException(nameof(client));
        }

        public async Task<HttpResponseMessage> GetAllowedFormatsAsync()
        {
            return await _client.GetAsync("/datetime/formats");
        }

        public async Task<HttpResponseMessage> IsFormatAllowedAsync(string format)
        {
            return await _client.GetAsync($"/datetime/formats/isAllowed?format={format}");
        }
    }
}
