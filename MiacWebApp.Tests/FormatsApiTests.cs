﻿using FluentAssertions;
using Miac.Times.Services;
using MiacWebApp.Responses;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace MiacWebApp.Tests
{
    public abstract class FormatsApiTests : IDisposable
    {
        protected WebApplicationFactory<Startup> _factory;

        protected FormatsApiClient _client;

        public FormatsApiTests(WebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            _client = new FormatsApiClient(_factory.CreateClient());
        }

        [Fact]
        public async Task Formats_ByDefault_ReturnsAllowedFormats()
        {
            var response = await _client.GetAllowedFormatsAsync();
            var options = _factory.Services.GetRequiredService<IOptions<DateTimeFormatOptions>>().Value;

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var body = await response.Content.ReadAsStringAsync();

            body.Should().NotBeNullOrEmpty();

            var actualAllowedFormats = JsonConvert.DeserializeObject<AllowedFormatsResponse>(body);

            actualAllowedFormats.Allowed.Should().Equal(options.AllowedFormats);
        }

        [Theory]
        [InlineData("mmmmmmmm", false)]
        [InlineData("dd/MM/yyyy", true)]
        public async Task IsAllowed_ReturnsIfFormatAllowed(string format, bool expectedResponse)
        {
            var response = await _client.IsFormatAllowedAsync(format);

            response.StatusCode.Should().Be(HttpStatusCode.OK);

            var body = await response.Content.ReadAsStringAsync();

            bool actualResponse = bool.Parse(body);

            actualResponse.Should().Be(expectedResponse);
        }
        
        public void Dispose()
        {
            _factory.Dispose();
        }
    }
}
